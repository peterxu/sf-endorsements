import React from 'react';
import MainPage from './MainPage';
import Loading from './Loading';

/** A page that only displays a loading icon */
const LoadingPage: React.FunctionComponent<{}> = React.memo(
  function LoadingPage() {
    return (
      <MainPage>
        <div className="py-5 text-center">
          <Loading />
        </div>
      </MainPage>
    );
  },
);

export default LoadingPage;
