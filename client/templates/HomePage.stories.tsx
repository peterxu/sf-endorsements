import * as React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import HomePage from './HomePage';

const meta: Meta<typeof HomePage> = {
  component: HomePage,
  title: 'HomePage',
};

export default meta;
type Story = StoryObj<typeof HomePage>;

export const Basic: Story = {
  render: () => <HomePage />,
};
