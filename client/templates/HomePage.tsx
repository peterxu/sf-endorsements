import React from 'react';
import jump from 'jump.js';
import { Link } from 'react-router-dom';

import MainPage from './MainPage';
import { adminEmail } from '../../common/constants';
import Logo from './Logo';
import ElectionEndorsements from './ElectionEndorsements';
import electionData from '../data/Nov2024';

const electionTitle = 'November, 2024';

export interface HomePageProps {}

function handleJumpToEndorsements() {
  jump('#HomePage__contentStart');
}

const HomePage: React.FunctionComponent<HomePageProps> = React.memo(
  function HomePage() {
    return (
      <MainPage>
        <div className="HomePage__image">
          <div className="container">
            <Logo light />

            <div className="align-items-center py-5 HomePage__lead">
              <h2>
                We collected all the <strong>endorsements</strong> and{' '}
                <strong>voter guides</strong> for the {electionTitle} San
                Francisco local, state, and national elections{' '}
                <strong>in one place</strong>{' '}
                <span className="d-none d-lg-inline">
                  for every political affiliation
                </span>
              </h2>
              <button
                className="btn btn-primary HomePage__leadButton mt-4"
                type="button"
                onClick={handleJumpToEndorsements}
              >
                See all endorsements
              </button>
            </div>
          </div>
        </div>
        <div className="container py-5" id="HomePage__contentStart">
          <p>
            SF Endorsements is an independent, non-partisan source that shows
            endorsements from many organizations for the {electionTitle} San
            Francisco municipal elections and ballot propositions. Endorsements
            are roughly ranked by their rankings when I did a Google search for
            "SF endorsements". We're not affiliated with any of the
            organizations or sources listed.
          </p>
          <p>
            SF Endorsements is maintained by Peter Xu (co-founder of{' '}
            <a href="https://wanderlog.com" target="_blank">
              Wanderlog, a tool to plan vacation travel
            </a>
            ). Get in touch at <a href={`mailto:${adminEmail}`}>{adminEmail}</a>{' '}
            if you have any suggestions or if you find any inaccuracies!
          </p>

          <p>
            (Past endorsements:{' '}
            <Link to="/past/march-2024-sf-election-endorsements">
              March 2024
            </Link>
            ;{' '}
            <Link to="/past/november-2022-sf-election-endorsements">
              November 2022
            </Link>
            ;{' '}
            <Link to="/past/june-2022-sf-election-endorsements">June 2022</Link>
            ;{' '}
            <Link to="/past/february-2022-sf-election-endorsements">
              February 2022
            </Link>
            ;{' '}
            <Link to="/past/november-2020-sf-election-endorsements">
              November 2020
            </Link>
            ;{' '}
            <Link to="/past/march-2020-sf-election-endorsements">
              March 2020
            </Link>
            ;{' '}
            <Link to="/past/november-2019-sf-election-endorsements">
              November 2019
            </Link>
            )
          </p>
          <hr />
          <ElectionEndorsements
            electionTitle={electionTitle}
            data={electionData}
          />
        </div>
      </MainPage>
    );
  },
);

export default HomePage;
