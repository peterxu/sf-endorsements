import React from 'react';
import { PopoverHeader, PopoverBody, UncontrolledPopover } from 'reactstrap';
import { RaceMetadata } from '../../common/types';

interface Props {
  race: RaceMetadata;
  idSuffix: string;
}

export default class RaceName extends React.PureComponent<Props> {
  render() {
    const { race, idSuffix } = this.props;

    const buttonId = `RaceName__${race.name.replace(/\W+/g, '')}_${idSuffix}`;
    if (race.description) {
      return (
        <>
          <UncontrolledPopover
            target={buttonId}
            placement="bottom"
            trigger="legacy"
          >
            <PopoverHeader>{race.title || race.name}</PopoverHeader>
            <PopoverBody>
              {race.description}
              {race.link && (
                <div className="mt-2">
                  <a
                    href={race.link}
                    className="btn btn-primary"
                    target="_blank"
                    rel="nofollow"
                  >
                    Read more at about it
                  </a>
                </div>
              )}
            </PopoverBody>
          </UncontrolledPopover>

          <button
            className="btn btn-link text-left p-0"
            type="button"
            id={buttonId}
          >
            {race.name}
          </button>
        </>
      );
    } else {
      return race.name;
    }
  }
}
