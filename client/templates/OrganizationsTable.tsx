import React from 'react';
import { Organization } from '../../common/types';

interface Props {
  organizations: Organization[];
}

/** Table describing each of the organizations for which we have an endorsement */
export default class OrganizationsTable extends React.PureComponent<Props> {
  render() {
    const { organizations } = this.props;
    return (
      <table className="table mx-n2">
        <thead>
          <tr>
            <th>Organization and link</th>
            <th>Description</th>
          </tr>
        </thead>

        <tbody>
          {organizations.map((organization) => (
            <tr key={organization.name}>
              <td>
                <strong>
                  <a
                    href={organization.homepage}
                    target="_blank"
                    rel="nofollow"
                  >
                    {organization.name}
                  </a>
                </strong>
              </td>
              <td>{organization.description}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}
