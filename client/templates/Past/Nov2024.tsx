import React from 'react';
import electionData from '../../data/Nov2024';
import PastElection from './PastElection';

const electionTitle = 'November, 2024';

/** Results for November 2024 election */
const Nov2024: React.FunctionComponent<{}> = React.memo(function Nov2024() {
  return <PastElection electionTitle={electionTitle} data={electionData} />;
});

export default Nov2024;
