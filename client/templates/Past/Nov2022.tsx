import React from 'react';
import electionData from '../../data/Nov2022';
import PastElection from './PastElection';

const electionTitle = 'November, 2022';

/** Results for November 2022 election */
const Nov2022: React.FunctionComponent<{}> = React.memo(function Nov2022() {
  return <PastElection electionTitle={electionTitle} data={electionData} />;
});

export default Nov2022;
