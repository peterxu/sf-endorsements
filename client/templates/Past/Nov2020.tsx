import React from 'react';
import electionData from '../../data/Nov2020';
import PastElection from './PastElection';

export interface Nov2020Props {}

const electionTitle = 'November, 2020';

/** Results for November 2020 election */
const Nov2020: React.FunctionComponent<Nov2020Props> = React.memo(
  function Nov2020() {
    return <PastElection electionTitle={electionTitle} data={electionData} />;
  },
);

export default Nov2020;
