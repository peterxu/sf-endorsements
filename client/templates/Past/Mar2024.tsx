import React from 'react';
import electionData from '../../data/Mar2024';
import PastElection from './PastElection';

const electionTitle = 'March, 2024';

/** Results for March 2024 election */
const Mar2024: React.FunctionComponent<{}> = React.memo(function Mar2024() {
  return <PastElection electionTitle={electionTitle} data={electionData} />;
});

export default Mar2024;
