import React from 'react';
import {
  Organization,
  OrgType,
  RaceMetadata,
  Section,
} from '../../../common/types';
import Endorsements from '../Endorsements';
import MainPageWithHeader from '../MainPageWithHeader';

enum Race {
  democraticPresident = 'democraticPresident',
  mayor = 'mayor',
  districtAttorney = 'districtAttorney',
  sfJudge1 = 'sfJudge1',
  sfJudge18 = 'sfJudge18',
  sfJudge21 = 'sfJudge21',
  district5 = 'district5',
  boardOfEducation = 'boardOfEducation',

  propA = 'propA',
  propB = 'propB',
  propC = 'propC',
  propD = 'propD',
  propE = 'propE',
  propF = 'propF',

  prop13 = 'prop13',
}

const organizations: Organization[] = [
  {
    name: 'San Francisco Chronicle',
    description:
      'Major San Francisco Bay Area daily newspaper with the largest average daily circulation',
    type: OrgType.newspaper,
    homepage: 'https://www.sfchronicle.com/',
    link: 'https://projects.sfchronicle.com/2019/voter-guide/',
    propositionsLink: 'https://projects.sfchronicle.com/2019/voter-guide/',
    endorsements: {
      [Race.mayor]: 'London Breed',
      [Race.districtAttorney]: 'Suzy Loftus',
      [Race.district5]: 'Vallie Brown',
      [Race.boardOfEducation]: 'Jenny Lam',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'No',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'San Francisco Examiner',
    type: OrgType.newspaper,
    description: 'Free San Francisco daily newspaper published since 1863',
    homepage: 'https://www.sfexaminer.com/',
    link: 'https://www.sfexaminer.com/opinion/our-endorsements-for-san-franciscos-top-offices/',
    propositionsLink:
      'https://www.sfexaminer.com/opinion/here-are-the-examiners-recommendations-for-the-november-ballots-local-measures/',
    endorsements: {
      [Race.mayor]: 'London Breed',
      [Race.districtAttorney]: '',
      [Race.district5]: 'Vallie Brown',
      [Race.boardOfEducation]: 'Jenny Lam',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'Democratic Party (SF)',
    type: OrgType.party,
    description:
      'Local branch of the Democratic Party, the dominant party in San Francisco and the more liberal/left-leaning of the two major US parties',
    homepage: 'https://www.sfdemocrats.org/',
    link: 'http://www.sfdemocrats.org/voting/endorsements/2019/8/16/november-5-2019-general-election',
    propositionsLink:
      'http://www.sfdemocrats.org/voting/endorsements/2019/8/16/november-5-2019-general-election',
    endorsements: {
      [Race.mayor]: 'London Breed',
      [Race.districtAttorney]: 'Suzy Loftus',
      [Race.district5]: 'Vallie Brown',
      [Race.boardOfEducation]: 'Jenny Lam',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'San Francisco Bay Guardian',
    type: OrgType.newspaper,
    description:
      'Online-only newspaper that reports and promotes left-wing, progressive issues',
    homepage: 'http://www.sfbg.com/',
    link: 'http://www.sfbg.com/2019/10/07/endorsements-2019/',
    propositionsLink: 'http://www.sfbg.com/2019/10/07/endorsements-2019/',
    endorsements: {
      [Race.districtAttorney]: 'Chesa Boudin',
      [Race.district5]: 'Dean Preston',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'SF Tenants Union',
    type: OrgType.nonProfit,
    description:
      'Non-profit that advocates for and educates renters and tenants on their rights',
    homepage: 'https://www.sftu.org/',
    link: 'https://www.sftu.org/endorsements/',
    propositionsLink: 'https://www.sftu.org/endorsements/',
    endorsements: {
      [Race.district5]: 'Dean Preston',
      [Race.districtAttorney]: 'Chesa Boudin',
      [Race.mayor]: ['Mark Leno', 'Jane Kim'],
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'Sierra Club',
    type: OrgType.nonProfit,
    description:
      "The San Francisco Bay Chapter is the local branch of the Sierra Club, America's largest and most effective grassroots environmental organization",
    homepage: 'https://www.sierraclub.org/san-francisco-bay',
    link: 'https://www.sierraclub.org/san-francisco-bay/november2019endorsements',
    propositionsLink:
      'https://www.sierraclub.org/san-francisco-bay/november2019endorsements',
    endorsements: {
      [Race.district5]: 'Dean Preston',
      [Race.districtAttorney]: ['Chesa Boudin', 'Leif Dautch'],
      [Race.propA]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'SF League of Pissed Off Voters',
    type: OrgType.nonProfit,
    description:
      'A group of political enthusiasts formed with the goal of building a progressive majority in San Francisco government',
    homepage: 'http://www.theleaguesf.org/',
    link: 'http://www.theleaguesf.org/',
    propositionsLink: 'http://www.theleaguesf.org/',
    endorsements: {
      [Race.district5]: 'Dean Preston',
      [Race.districtAttorney]: 'Chesa Boudin',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'Green Party (SF)',
    type: OrgType.party,
    description:
      'SF chapter of the national Green Party: part of the Green movement that works to encourage non-violence, ecological respect, grassroots democracy, and social justice',
    homepage: 'https://www.sfgreenparty.org/',
    link: 'https://www.sfgreenparty.org/endorsements',
    propositionsLink: 'https://www.sfgreenparty.org/endorsements',
    endorsements: {
      [Race.mayor]: 'Joel Ventresca',
      [Race.districtAttorney]: 'Chesa Boudin',
      [Race.district5]: ['Dean Preston', 'Vallie Brown'],
      [Race.propA]: 'No',
      [Race.propC]: 'No',
      [Race.propD]: 'No',
      [Race.propE]: 'No',
      [Race.propF]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'YIMBY Action',
    type: OrgType.nonProfit,
    description:
      'Pro-housing advocates trying to increase supply of housing and bring down cost of living',
    homepage: 'https://yimbyaction.org/',
    link: 'https://medium.com/@sfyimby/how-to-vote-pro-housing-in-san-francisco-this-november-22384514ac92',
    propositionsLink:
      'https://medium.com/@sfyimby/how-to-vote-pro-housing-in-san-francisco-this-november-22384514ac92',
    endorsements: {
      [Race.mayor]: 'London Breed',
      [Race.district5]: 'Vallie Brown',
      [Race.propA]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'No',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'SF Labor Council',
    type: OrgType.nonProfit,
    description:
      'Local body of the AFL-CIO labor union federation: consists of 150 unions representing 100,000 members in the Bay Area',
    homepage: 'http://sflaborcouncil.org/',
    link: 'http://sflaborcouncil.org/politics/endorsements/',
    propositionsLink: 'http://sflaborcouncil.org/politics/endorsements/',
    endorsements: {
      [Race.mayor]: 'London Breed',
      [Race.boardOfEducation]: 'Jenny Lam',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'SF League of Conservation Voters',
    type: OrgType.nonProfit,
    description:
      'Non-profit that seeks to protect and improve the local environment, including public transit, public spaces, natural resources, and sustainability',
    homepage: 'https://www.sflcv.org/',
    link: 'https://www.sflcv.org/endorsements',
    propositionsLink: 'https://www.sflcv.org/endorsements',
    endorsements: {
      [Race.district5]: 'Vallie Brown',
      [Race.districtAttorney]: ['Chesa Boudin', 'Leif Dautch', 'Suzy Loftus'],
      [Race.boardOfEducation]: 'Jenny Lam',
      [Race.propA]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: "SF Women's Political Committee",
    type: OrgType.nonProfit,
    description:
      "Largest women's organization in SF that promotes women in leadership, gender parity, and women's issues",
    homepage: 'https://sfwpc.org/',
    link: 'https://sfwpc.org/endorsements/',
    propositionsLink: 'https://sfwpc.org/endorsements/',
    endorsements: {
      [Race.mayor]: 'London Breed',
      [Race.district5]: 'Vallie Brown',
      [Race.districtAttorney]: 'Suzy Loftus',
      [Race.boardOfEducation]: 'Jenny Lam',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'SF Berniecrats',
    type: OrgType.nonProfit,
    description:
      'Group founded by Bernie Sanders supporters from the 2016 election to support a populist, progressive movement on the Left',
    homepage: 'https://sfberniecrats.com/',
    link: 'https://sfberniecrats.com/endorsements/2019-endorsements/',
    propositionsLink:
      'https://sfberniecrats.com/endorsements/2019-endorsements/',
    endorsements: {
      [Race.district5]: 'Dean Preston',
      [Race.districtAttorney]: 'Chesa Boudin',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'SPUR (Planning / Urban Research Association)',
    type: OrgType.nonProfit,
    description:
      'A civic planning organization that promotes good urban planning (affordable housing, controlled sprawl, better transit) in the Bay Area',
    homepage: 'https://www.spur.org/',
    link: 'https://www.spur.org/voter-guide/san-francisco-2019-10',
    propositionsLink: 'https://www.spur.org/voter-guide/san-francisco-2019-10',
    endorsements: {
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'No',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'League of Women Voters of SF',
    type: OrgType.nonProfit,
    description:
      'Organization that encourages voting and informed participation in politics: formed originally as part of suffrage movement in early 1900s',
    homepage: 'https://lwvsf.org/',
    link: 'https://lwvsf.org/ballot-recommendations',
    propositionsLink: 'https://lwvsf.org/ballot-recommendations',
    endorsements: {
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'SF Bicycle Coalition',
    type: OrgType.nonProfit,
    description:
      'Bicycle education and advocacy group that aims to make San Francisco a more bicycle-friendly city',
    homepage: 'https://sfbike.org/',
    link: 'https://sfbike.org/news/our-endorsements-november-5-2019-elections/',
    propositionsLink:
      'https://sfbike.org/news/our-endorsements-november-5-2019-elections/',
    endorsements: {
      [Race.mayor]: 'London Breed',
      [Race.districtAttorney]: 'Suzy Loftus',
      [Race.propD]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'SF Republican Party',
    type: OrgType.party,
    description:
      'Local branch of the Republican Party, the more conservative/right-leaning of the two major US parties',
    homepage: 'http://www.sfgop.org/',
    link: 'http://www.sfgop.org/',
    propositionsLink: 'http://www.sfgop.org/',
    endorsements: {
      [Race.districtAttorney]: 'Nancy Tung',
      [Race.boardOfEducation]: 'Jenny Lam',
      [Race.propA]: 'No',
      [Race.propD]: 'No',
      [Race.propF]: 'No',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'Harvey Milk LGBTQ Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Party club that supports LGBTQ and progressive issues and people of marginalized backgrounds',
    homepage: 'http://www.milkclub.org/',
    link: 'http://www.milkclub.org/endorsements',
    propositionsLink: 'http://www.milkclub.org/endorsements',
    endorsements: {
      [Race.district5]: 'Dean Preston',
      [Race.districtAttorney]: 'Chesa Boudin',
      [Race.boardOfEducation]: 'Jenny Lam',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'United Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Party club founded in 2016 that represents a diverse group of Democrats advancing goals of social justice, protecting the vulnerable, and providing economic opportunity for all',
    homepage: 'https://www.uniteddems.org/',
    link: 'https://www.uniteddems.org/elections',
    propositionsLink: 'https://www.uniteddems.org/elections',
    endorsements: {
      [Race.mayor]: 'London Breed',
      [Race.district5]: 'Vallie Brown',
      [Race.districtAttorney]: 'Suzy Loftus',
      [Race.boardOfEducation]: 'Jenny Lam',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'No',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'Ed M. Lee Asian Pacific Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Party club that hopes to engage Asian Pacific Islanders (API) and support API leaders',
    homepage: 'https://www.edleedems.org/',
    link: 'https://www.edleedems.org/endorsements/nov-5-2019-election-endorsements',
    propositionsLink:
      'https://www.edleedems.org/endorsements/nov-5-2019-election-endorsements',
    endorsements: {
      [Race.mayor]: 'London Breed',
      [Race.district5]: 'Vallie Brown',
      [Race.districtAttorney]: 'Suzy Loftus',
      [Race.boardOfEducation]: 'Jenny Lam',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'No',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'Alice B. Toklas LGBT Democratic Club',
    type: OrgType.nonProfit,
    description:
      'First LGBT-centered Democratic Party club in the country; advocates for LGBT issues and candidates',
    homepage: 'http://www.alicebtoklas.org/about/',
    link: 'http://www.alicebtoklas.org/2019/08/november19endorsements/',
    propositionsLink:
      'http://www.alicebtoklas.org/2019/08/november19endorsements/',
    endorsements: {
      [Race.mayor]: 'London Breed',
      [Race.district5]: 'Vallie Brown',
      [Race.districtAttorney]: 'Suzy Loftus',
      [Race.boardOfEducation]: 'Jenny Lam',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'No',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.propF]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
];

export const candidateRaces: RaceMetadata[] = [
  { race: Race.mayor, name: 'Mayor' },
  { race: Race.districtAttorney, name: 'District attorney' },
  { race: Race.district5, name: 'Supervisor, District 5' },
  { race: Race.boardOfEducation, name: 'Board of Education' },
];

export const propositions: RaceMetadata[] = [
  {
    race: Race.propA,
    name: 'Prop A',
    title: 'Affordable Housing Bond',
    link: 'https://voterguide.sfelections.org/en/affordable-housing-bond',
    description:
      'Authorization to issue $600 million in bonds to build and maintain affordable housing',
  },
  {
    race: Race.propB,
    name: 'Prop B',
    title: 'Dept. of Disability and Aging Services',
    link: 'https://voterguide.sfelections.org/en/department-disability-and-aging-services',
    description:
      'Rename department from "Aging and Adult Services Commission" to "Dept. of Disability and Aging Services" and require 3 or the Commission\'s seats to be reserved for a person >60 years of age, with a disability, and who has served in the military respectively',
  },
  {
    race: Race.propC,
    name: 'Prop C',
    title: 'Vapor Products',
    description:
      'Allow e-cigarettes sales back into San Francisco by overturning a law passed earlier this year by the Board of Supervisors (city council)',
    link: 'https://voterguide.sfelections.org/en/vapor-products',
  },
  {
    race: Race.propD,
    name: 'Prop D',
    title: 'Traffic Congestion Mitigation Tax',
    description:
      'Add 1.5% tax on shared rides (Uber Pool/Lyft Line/etc.) and 3.25% tax on private rides (regular Uber/Lyft) to fund Muni transit and bicycle/pedestrian safety',
    link: 'https://voterguide.sfelections.org/en/traffic-congestion-mitigation-tax',
  },
  {
    race: Race.propE,
    name: 'Prop E',
    title: 'Affordable Housing and Educator Housing',
    description:
      'Change zoning laws to make it easier to build Affordable/Educator Housing in existing residential and public zoning districts',
    link: 'https://voterguide.sfelections.org/en/affordable-housing-and-educator-housing',
  },
  {
    race: Race.propF,
    name: 'Prop F',
    title: 'Campaign Contrib. and Campaign Ads',
    description:
      'Require PACs (private campaign associations) to mention their top 3 donors (if they donated >$10,000) in ads, and limit contributions from property developers and others with land use interests to campaigns',
    link: 'https://voterguide.sfelections.org/en/campaign-contributions-and-campaign-advertisements',
  },
];

const sections: Section[] = [
  {
    heading: 'Candidate races',
    subtitle: 'Only contested races shown',
    metadata: candidateRaces,
  },
  { heading: 'Propositions and ballot initiatives', metadata: propositions },
];

const Nov2019: React.FunctionComponent<{}> = React.memo(function Nov2019() {
  return (
    <MainPageWithHeader>
      <div className="container py-5" id="HomePage__contentStart">
        <h1>Endorsements for the November, 2019 election</h1>

        <Endorsements
          electionTitle="November 2019"
          sections={sections}
          organizations={organizations}
        />
      </div>
    </MainPageWithHeader>
  );
});

export default Nov2019;
