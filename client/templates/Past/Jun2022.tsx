import React from 'react';
import electionData from '../../data/Jun2022';
import PastElection from './PastElection';

const electionTitle = 'June, 2022';

/** Results for June 2022 election */
const Jun2022: React.FunctionComponent<{}> = React.memo(function Jun2022() {
  return <PastElection electionTitle={electionTitle} data={electionData} />;
});

export default Jun2022;
