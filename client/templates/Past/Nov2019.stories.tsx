import * as React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Nov2019 from './Nov2019';

const meta: Meta<typeof Nov2019> = {
  component: Nov2019,
  title: 'Nov2019',
};

export default meta;
type Story = StoryObj<typeof Nov2019>;

export const Basic: Story = {
  render: () => <Nov2019 />,
};
