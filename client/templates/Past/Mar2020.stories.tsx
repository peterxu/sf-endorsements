import * as React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Mar2020 from './Mar2020';

const meta: Meta<typeof Mar2020> = {
  component: Mar2020,
  title: 'Mar2020',
};

export default meta;
type Story = StoryObj<typeof Mar2020>;

export const Basic: Story = {
  render: () => <Mar2020 />,
};
