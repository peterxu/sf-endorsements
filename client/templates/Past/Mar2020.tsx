import React from 'react';
import {
  Organization,
  OrgType,
  RaceMetadata,
  Section,
} from '../../../common/types';
import DeprecatedPastElection from './DeprecatedPastElection';

enum Race {
  democraticPresident = 'democraticPresident',
  mayor = 'mayor',
  districtAttorney = 'districtAttorney',
  sfJudge1 = 'sfJudge1',
  sfJudge18 = 'sfJudge18',
  sfJudge21 = 'sfJudge21',
  district5 = 'district5',
  boardOfEducation = 'boardOfEducation',

  propA = 'propA',
  propB = 'propB',
  propC = 'propC',
  propD = 'propD',
  propE = 'propE',
  propF = 'propF',

  prop13 = 'prop13',
}

const organizations: Organization[] = [
  {
    name: 'San Francisco Chronicle',
    description:
      'Major San Francisco Bay Area daily newspaper with the largest average daily circulation',
    type: OrgType.newspaper,
    homepage: 'https://www.sfchronicle.com/',
    link: 'https://projects.sfchronicle.com/2020/voter-guide/',
    propositionsLink: 'https://projects.sfchronicle.com/2020/voter-guide/',
    endorsements: {
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'No',
      [Race.sfJudge1]: 'Pang Ly',
      [Race.sfJudge18]: 'Dorothy Chou Proudfoot',
      [Race.sfJudge21]: 'Kulvindar “Rani” Singh',
      [Race.prop13]: 'Yes',
    },
    hasReasoning: true,
    hasNonPoliticalActivities: true,
  },
  // {
  //   name: 'San Francisco Examiner',
  //   type: OrgType.newspaper,
  //   description: 'Free San Francisco daily newspaper published since 1863',
  //   homepage: 'https://www.sfexaminer.com/',
  //   link:
  //     'https://www.sfexaminer.com/opinion/our-endorsements-for-san-franciscos-top-offices/',
  //   propositionsLink:
  //     'https://www.sfexaminer.com/opinion/here-are-the-examiners-recommendations-for-the-november-ballots-local-measures/',
  //   endorsements: {

  //   },
  // },
  {
    name: 'Democratic Party (SF)',
    type: OrgType.party,
    description:
      'Local branch of the Democratic Party, the dominant party in San Francisco and the more liberal/left-leaning of the two major US parties',
    homepage: 'https://www.sfdemocrats.org/',
    link: 'https://www.sfdemocrats.org/voting/endorsements/march2020',
    propositionsLink:
      'https://www.sfdemocrats.org/voting/endorsements/march2020',
    endorsements: {
      [Race.sfJudge1]: 'Maria Evangelista',
      [Race.sfJudge18]: 'Michelle Tong',
      [Race.sfJudge21]: 'Carolyn Gold',
      [Race.prop13]: 'Yes',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'San Francisco Bay Guardian',
    type: OrgType.newspaper,
    description:
      'Online-only newspaper that reports and promotes left-wing, progressive issues',
    homepage: 'http://www.sfbg.com/',
    link: 'http://www.sfbg.com/2020/02/10/endorsements-for-the-march-3-primary/',
    propositionsLink:
      'http://www.sfbg.com/2020/02/10/endorsements-for-the-march-3-primary/',
    endorsements: {
      [Race.democraticPresident]: 'Bernie Sanders',
      [Race.sfJudge1]: 'Maria Evangelista',
      [Race.sfJudge18]: 'Michelle Tong',
      [Race.sfJudge21]: 'Carolyn Gold',
      [Race.prop13]: 'Yes',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
    },
    hasReasoning: true,
    hasNonPoliticalActivities: true,
  },
  {
    name: 'Sierra Club',
    type: OrgType.nonProfit,
    description:
      "The San Francisco Bay Chapter is the local branch of the Sierra Club, America's largest and most effective grassroots environmental organization",
    homepage: 'https://www.sierraclub.org/san-francisco-bay',
    link: 'https://www.sierraclub.org/san-francisco-bay/march-2020-endorsements',
    propositionsLink:
      'https://www.sierraclub.org/san-francisco-bay/march-2020-endorsements',
    endorsements: {
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: true,
  },
  {
    name: 'Green Party (SF)',
    type: OrgType.party,
    description:
      'SF chapter of the national Green Party: part of the Green movement that works to encourage non-violence, ecological respect, grassroots democracy, and social justice',
    homepage: 'https://www.sfgreenparty.org/',
    link: 'https://www.sfgreenparty.org/endorsements/91-march-2020-endorsements',
    propositionsLink:
      'https://www.sfgreenparty.org/endorsements/91-march-2020-endorsements',
    endorsements: {
      [Race.sfJudge1]: 'Maria Evangelista',
      [Race.sfJudge18]: ['Dorothy Chou Proudfoot', 'Michelle Tong'],
      [Race.sfJudge21]: ['Kulvindar “Rani” Singh', 'Carolyn Gold'],
      [Race.propA]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'No',
    },
    hasReasoning: true,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'YIMBY Action',
    type: OrgType.nonProfit,
    description:
      'Pro-housing advocates trying to increase supply of housing and bring down cost of living',
    homepage: 'https://yimbyaction.org/',
    link: 'https://medium.com/@sfyimby/sf-yimby-endorsements-for-march-2020-a3c188527940',
    propositionsLink:
      'https://medium.com/@sfyimby/sf-yimby-endorsements-for-march-2020-a3c188527940',
    endorsements: {
      [Race.propB]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'No',
    },
    hasReasoning: true,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'SF Tenants Union',
    type: OrgType.nonProfit,
    description:
      'Non-profit that advocates for and educates renters and tenants on their rights',
    homepage: 'https://www.sftu.org/',
    link: 'https://www.sftu.org/endorsements/',
    propositionsLink: 'https://www.sftu.org/endorsements/',
    endorsements: {
      [Race.democraticPresident]: 'Bernie Sanders',
      [Race.sfJudge1]: 'Maria Evangelista',
      [Race.sfJudge18]: 'Michelle Tong',
      [Race.sfJudge21]: 'Carolyn Gold',
      [Race.prop13]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: true,
  },
  {
    name: 'SF Housing Action Coalition',
    type: OrgType.nonProfit,
    description:
      'Pro-housing organization advocating for building new well-designed, well-located housing at all levels of affordability',
    homepage: 'https://www.sfhac.org/',
    link: 'https://www.sfhac.org/march-2020-election-endorsements/',
    propositionsLink: 'https://www.sfhac.org/march-2020-election-endorsements/',
    endorsements: {
      [Race.propE]: 'No',
    },
    hasReasoning: true,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'SF League of Pissed Off Voters',
    type: OrgType.nonProfit,
    description:
      'A group of political enthusiasts formed with the goal of building a progressive majority in San Francisco government',
    homepage: 'http://www.theleaguesf.org/',
    link: 'http://www.theleaguesf.org/',
    propositionsLink: 'http://www.theleaguesf.org/',
    endorsements: {
      [Race.sfJudge1]: 'Maria Evangelista',
      [Race.sfJudge18]: 'Michelle Tong',
      [Race.sfJudge21]: 'Carolyn Gold',
      [Race.prop13]: 'Yes',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
    },
    hasReasoning: true,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'SF Labor Council',
    type: OrgType.union,
    description:
      'Local body of the AFL-CIO labor union federation: consists of 150 unions representing 100,000 members in the Bay Area',
    homepage: 'http://sflaborcouncil.org/',
    link: 'http://sflaborcouncil.org/politics/endorsements/',
    propositionsLink: 'http://sflaborcouncil.org/politics/endorsements/',
    endorsements: {
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: true,
  },
  {
    name: 'SEIU 1021',
    type: OrgType.union,
    description:
      'Local chapter of the Service Employers International Union, representing 60,000 members in Northern California',
    homepage: 'https://www.seiu1021.org/',
    link: 'https://www.seiu1021.org/post/2020-march-endorsements',
    propositionsLink: 'https://www.seiu1021.org/post/2020-march-endorsements',
    endorsements: {
      [Race.sfJudge1]: 'Maria Evangelista',
      [Race.sfJudge18]: 'Michelle Tong',
      [Race.sfJudge21]: 'Carolyn Gold',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: true,
  },
  {
    name: 'SF League of Conservation Voters',
    type: OrgType.nonProfit,
    description:
      'Non-profit that seeks to protect and improve the local environment, including public transit, public spaces, natural resources, and sustainability',
    homepage: 'https://www.sflcv.org/',
    link: 'https://www.sflcv.org/endorsements',
    propositionsLink: 'https://www.sflcv.org/endorsements',
    endorsements: {
      [Race.propD]: 'Yes',
    },
    hasReasoning: true,
    hasNonPoliticalActivities: false,
  },
  {
    name: "SF Women's Political Committee",
    type: OrgType.nonProfit,
    description:
      "Largest women's organization in SF that promotes women in leadership, gender parity, and women's issues",
    homepage: 'https://sfwpc.org/',
    link: 'https://sfwpc.org/endorsements/',
    propositionsLink: 'https://sfwpc.org/endorsements/',
    endorsements: {
      [Race.sfJudge1]: 'Maria Evangelista',
      [Race.sfJudge18]: 'Dorothy Chou Proudfoot',
      [Race.sfJudge21]: 'Kulvindar “Rani” Singh',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
    },
    hasReasoning: true,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'SF Berniecrats',
    type: OrgType.nonProfit,
    description:
      'Group founded by Bernie Sanders supporters from the 2016 election to support a populist, progressive movement on the Left',
    homepage: 'https://sfberniecrats.com/',
    link: 'https://sfberniecrats.com/endorsements/march-2020-endorsements/',
    propositionsLink:
      'https://sfberniecrats.com/endorsements/march-2020-endorsements/',
    endorsements: {
      [Race.democraticPresident]: 'Bernie Sanders',
      [Race.sfJudge1]: 'Maria Evangelista',
      [Race.sfJudge18]: 'Michelle Tong',
      [Race.sfJudge21]: 'Carolyn Gold',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.prop13]: 'Yes',
    },
    hasReasoning: true,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'SPUR (Planning / Urban Research Association)',
    type: OrgType.nonProfit,
    description:
      'A civic planning organization that promotes good urban planning (affordable housing, controlled sprawl, better transit) in the Bay Area',
    homepage: 'https://www.spur.org/',
    link: 'https://www.spur.org/voter-guide/san-francisco-2020-03',
    propositionsLink: 'https://www.spur.org/voter-guide/san-francisco-2020-03',
    endorsements: {
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'No',
    },
    hasReasoning: true,
    hasNonPoliticalActivities: true,
  },
  {
    name: 'League of Women Voters of SF',
    type: OrgType.nonProfit,
    description:
      'Organization that encourages voting and informed participation in politics: formed originally as part of suffrage movement in early 1900s',
    homepage: 'https://lwvsf.org/',
    link: 'https://lwvsf.org/ballot-recommendations',
    propositionsLink: 'https://lwvsf.org/ballot-recommendations',
    endorsements: {
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
    },
    hasReasoning: true,
    hasNonPoliticalActivities: true,
  },
  // {
  //   name: 'SF Bicycle Coalition',
  //   type: OrgType.nonProfit,
  //   description:
  //     'Bicycle education and advocacy group that aims to make San Francisco a more bicycle-friendly city',
  //   homepage: 'https://sfbike.org/',
  //   link: 'https://sfbike.org/news/our-endorsements-november-5-2019-elections/',
  //   propositionsLink:
  //     'https://sfbike.org/news/our-endorsements-november-5-2019-elections/',
  //   endorsements: {
  //   },
  // },
  {
    name: 'SF Republican Party',
    type: OrgType.party,
    description:
      'Local branch of the Republican Party, the more conservative/right-leaning of the two major US parties',
    homepage: 'http://www.sfgop.org/',
    link: 'http://www.sfgop.org/',
    propositionsLink: 'http://www.sfgop.org/',
    endorsements: {
      [Race.sfJudge1]: 'Pang Ly',
      [Race.sfJudge18]: 'Dorothy Chou Proudfoot',
      [Race.propA]: 'No',
      [Race.propD]: 'No',
      [Race.propE]: 'No',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'District 11 Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Party club representing SF District 11, covering Excelsior, Ocean View, Ingleside, and Visitacion Valley',
    homepage: 'https://www.phdemclub.org/',
    link: 'https://www.phdemclub.org/?p=4947',
    propositionsLink: 'https://www.phdemclub.org/?p=4947',
    endorsements: {
      [Race.sfJudge1]: 'Maria Evangelista',
      [Race.sfJudge18]: 'Michelle Tong',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'Potrero Hill Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Party club founded in 2007 representing Potrero Hill',
    homepage: 'https://www.phdemclub.org/',
    link: 'https://www.phdemclub.org/?p=4947',
    propositionsLink: 'https://www.phdemclub.org/?p=4947',
    endorsements: {
      [Race.democraticPresident]: 'Elizabeth Warren',
      [Race.sfJudge18]: 'Dorothy Chou Proudfoot',
      [Race.sfJudge21]: 'Kulvindar “Rani” Singh',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.prop13]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'Harvey Milk LGBTQ Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Party club that supports LGBTQ and progressive issues and people of marginalized backgrounds',
    homepage: 'http://www.milkclub.org/',
    link: 'http://www.milkclub.org/2020_endorsements',
    propositionsLink: 'http://www.milkclub.org/2020_endorsements',
    endorsements: {
      [Race.sfJudge1]: 'Maria Evangelista',
      [Race.sfJudge18]: 'Michelle Tong',
      [Race.sfJudge21]: 'Carolyn Gold',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
      [Race.prop13]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'United Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Party club founded in 2016 that represents a diverse group of Democrats advancing goals of social justice, protecting the vulnerable, and providing economic opportunity for all',
    homepage: 'https://www.uniteddems.org/',
    link: 'https://www.uniteddems.org/elections',
    propositionsLink: 'https://www.uniteddems.org/elections',
    endorsements: {
      [Race.sfJudge1]: 'Pang Ly',
      [Race.sfJudge18]: 'Dorothy Chou Proudfoot',
      [Race.sfJudge21]: 'Kulvindar “Rani” Singh',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propE]: 'No',
      [Race.prop13]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'Ed M. Lee Asian Pacific Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Party club that hopes to engage Asian Pacific Islanders (API) and support API leaders',
    homepage: 'https://www.edleedems.org/',
    link: 'https://www.edleedems.org/endorsements/march2020',
    propositionsLink: 'https://www.edleedems.org/endorsements/march2020',
    endorsements: {
      [Race.sfJudge1]: 'Pang Ly',
      [Race.sfJudge18]: 'Dorothy Chou Proudfoot',
      [Race.sfJudge21]: 'Kulvindar “Rani” Singh',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'No',
      [Race.propE]: 'No',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'Alice B. Toklas LGBT Democratic Club',
    type: OrgType.nonProfit,
    description:
      'First LGBT-centered Democratic Party club in the country; advocates for LGBT issues and candidates',
    homepage: 'http://www.alicebtoklas.org/about/',
    link: 'http://www.alicebtoklas.org/2020/01/march-3-2020-endorsements/',
    propositionsLink:
      'http://www.alicebtoklas.org/2020/01/march-3-2020-endorsements/',
    endorsements: {
      [Race.sfJudge1]: 'Maria Evangelista',
      [Race.sfJudge21]: 'Kulvindar “Rani” Singh',
      [Race.prop13]: 'Yes',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
  {
    name: 'Bernal Heights Democratic Club',
    type: OrgType.nonProfit,
    description:
      'Democratic Club founded in 1988 in the Bernal Heights neighborhood of San Francisco',
    homepage: 'http://bhdemocrats.org/',
    link: 'http://bhdemocrats.org/index.php/endorsements/',
    propositionsLink: 'http://bhdemocrats.org/index.php/endorsements/',
    endorsements: {
      [Race.democraticPresident]: 'Bernie Sanders',
      [Race.sfJudge1]: 'Maria Evangelista',
      [Race.sfJudge18]: 'Michelle Tong',
      [Race.sfJudge21]: 'Carolyn Gold',
      [Race.prop13]: 'Yes',
      [Race.propA]: 'Yes',
      [Race.propB]: 'Yes',
      [Race.propC]: 'Yes',
      [Race.propD]: 'Yes',
      [Race.propE]: 'Yes',
    },
    hasReasoning: false,
    hasNonPoliticalActivities: false,
  },
];

export const candidateRaces: RaceMetadata[] = [
  {
    race: Race.democraticPresident,
    name: 'Dem. Party pres.',
  },
  { race: Race.sfJudge1, name: 'Judicial Seat 1' },
  { race: Race.sfJudge18, name: 'Judicial Seat 18' },
  { race: Race.sfJudge21, name: 'Judicial Seat 21' },
];

export const propositions: RaceMetadata[] = [
  {
    race: Race.propA,
    name: 'Prop A',
    title: 'City College Bonds',
    link: 'https://voterguide.sfelections.org/en/affordable-housing-bond',
    description:
      "Authorize issuing $845 million in bonds and levy a $1.1/$10,000 property tax until 2053 to pay for City College's repairs, seismic retrofits, and new buildings/equipment.",
  },
  {
    race: Race.propB,
    name: 'Prop B',
    title: 'Earthquake Safety and Emergency Services Bonds',
    link: 'https://voterguide.sfelections.org/en/san-francisco-earthquake-safety-and-emergency-response-bond-2020',
    description:
      'Authorize issuing $629 million in bonds and levy a $1.5/$10,000 property tax to pay for water pipes/infrastructure, 911 Call Center, and fire and police facilities.',
  },
  {
    race: Race.propC,
    name: 'Prop C',
    title: 'SF Housing Authority Retiree Health Benefits',
    description:
      'Ensure employees of the SF Housing Authority, which was recently absorbed by the SF city government, will enjoy retirement medical benefits reflecting when they started working at the Housing Authority.',
    link: 'https://voterguide.sfelections.org/en/retiree-health-care-benefits-former-employees-san-francisco-housing-authority',
  },
  {
    race: Race.propD,
    name: 'Prop D',
    title: 'Vacancy Tax',
    description:
      'Impose a $250/street foot tax on vacant, ground floor retail/commercial space, whose proceeds will be spent helping small businesses.',
    link: 'https://voterguide.sfelections.org/en/vacancy-tax',
  },
  {
    race: Race.propE,
    name: 'Prop E',
    title: 'Limits on Office Development',
    description:
      'Further limit large office development (>25,000 sq. ft.) if the city continue to miss State-set housing goals for affordable housing.',
    link: 'https://voterguide.sfelections.org/en/limits-office-development',
  },
  {
    race: Race.prop13,
    name: 'CA Prop 13',
    title: 'School and College Facilities Bond',
    description:
      'Authorize issuing $15 billion in state bonds for building and repairing schools and colleges ($9 billion for K-12, including charter schools; $4 billion for universities; $2 billion for community colleges).',
    link: 'https://voterguide.sos.ca.gov/propositions/13/title-summary.htm',
  },
];

const sections: Section[] = [
  {
    heading: 'Candidate races',
    subtitle: 'Only contested races shown',
    metadata: candidateRaces,
  },
  { heading: 'Propositions and ballot initiatives', metadata: propositions },
];

const Mar2020 = React.memo(function Mar2020() {
  return (
    <DeprecatedPastElection
      electionTitle="March 2020"
      sections={sections}
      organizations={organizations}
    />
  );
});

export default Mar2020;
