import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useCallback } from 'react';
import {
  UncontrolledPopover,
  PopoverHeader,
  PopoverBody,
  UncontrolledTooltip,
} from 'reactstrap';
import { RaceMetadata, Organization } from '../../common/types';
import EndorsementDisplay from './EndorsementDisplay';

interface EndorsementRowProps {
  /** ID suffix to add to Popover DOM IDs */
  tableId: string;
  organization: Organization;
  races: RaceMetadata[];

  /** Which key in the `organization` to use to link to the endorsements */
  linkKey: 'link' | 'propositionsLink';

  /** Map of candidate name to color, for easy browsing */
  colorMap: { [name: string]: string };
}

/**
 * A single row in an endorsement table, representing the endorsements
 * of one organization
 */
const EndorsementRow: React.FunctionComponent<EndorsementRowProps> = React.memo(
  function EndorsementRow(props: EndorsementRowProps) {
    const { organization, races, tableId, linkKey, colorMap } = props;

    const handleOpenEndorsementsLink = useCallback(() => {
      window.open(organization.link);
    }, [organization.link]);

    const buttonId = `EndorsementRow__${organization.name.replace(
      /\W+/g,
      '',
    )}_${tableId}`;
    const hasReasoningId = `EndorsementRow__${organization.name.replace(
      /\W+/g,
      '',
    )}_${tableId}_hasReasoning`;

    const hasEndorsements = races.some(
      (race) => organization.endorsements[race.race],
    );
    if (!hasEndorsements) return null;

    const handleStopPropagation = useCallback((e: React.MouseEvent) => {
      e.stopPropagation();
    }, []);

    return (
      <tr
        key={organization.name}
        className={
          organization.hasReasoning ? 'EndorsementRow__hasReasoning' : ''
        }
        onClick={
          organization.hasReasoning ? handleOpenEndorsementsLink : undefined
        }
      >
        <td
          style={{ position: 'sticky', left: 0 }}
          className="EndorsementRow__firstColumn"
        >
          <UncontrolledPopover
            trigger="legacy"
            placement="bottom"
            target={buttonId}
            fade
          >
            <PopoverHeader>{organization.name}</PopoverHeader>
            <PopoverBody>
              <div>{organization.description}</div>
              <div className="mt-2">
                <a
                  href={
                    linkKey === 'link'
                      ? organization.link
                      : organization.propositionsLink
                  }
                  className="btn btn-primary"
                  target="_blank"
                  rel="nofollow"
                >
                  Go to endorsements source
                </a>
              </div>
            </PopoverBody>
          </UncontrolledPopover>
          <button
            className="btn btn-link p-0 font-weight-bold text-left"
            type="button"
            id={buttonId}
            onClick={handleStopPropagation}
          >
            {organization.name}
          </button>
          {organization.hasReasoning && (
            <div>
              <UncontrolledTooltip
                target={hasReasoningId}
                delay={0}
                fade={false}
                placement="bottom"
              >
                This organization explains why they endorsed a certain position.
                Click to visit their site.
              </UncontrolledTooltip>
              <a
                href={organization.link}
                className="badge badge-light"
                // Make the badge stand out even with a row that might have
                // a gray background
                style={{ backgroundColor: '#ddd' }}
                id={hasReasoningId}
              >
                <FontAwesomeIcon icon={faInfoCircle} /> Has reasoning
              </a>
            </div>
          )}
        </td>
        {races.map((race) => {
          const endorsement = organization.endorsements[race.race];
          return (
            <td key={race.race}>
              {endorsement && (
                <EndorsementDisplay
                  endorsement={endorsement}
                  colorMap={colorMap}
                />
              )}
            </td>
          );
        })}
      </tr>
    );
  },
);

export default EndorsementRow;
