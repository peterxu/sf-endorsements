import * as React from 'react';
import { adminEmail, adminName } from '../../common/constants';

class MainPage extends React.PureComponent<React.PropsWithChildren<{}>> {
  render() {
    return (
      <>
        {this.props.children}

        <footer className="footer mb-5">
          <div className="container">
            <div>Created by {adminName}</div>
            <div>
              Email: <a href={`mailto:${adminEmail}`}>{adminEmail}</a>
            </div>
            <div>
              Code at{' '}
              <a
                href="https://www.gitlab.com/peterxu/sf-endorsements"
                rel="nofollow"
                target="_blank"
              >
                GitLab
              </a>
            </div>
          </div>
        </footer>
      </>
    );
  }
}

export default MainPage;
