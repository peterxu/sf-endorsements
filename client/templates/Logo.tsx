import React from 'react';
import { Link } from 'react-router-dom';

interface Props {
  /** Whether to make text light for overlaying on a dark background */
  light?: boolean;
}

export default class Logo extends React.PureComponent<Props> {
  render() {
    const { light } = this.props;
    return (
      <h1 className="d-flex">
        <Link to="/" className="Logo__link">
          <img
            src={light ? '/assets/logo.png' : '/assets/favicon.png'}
            alt=""
            className="Logo__image"
          />
          <span className="Logo__text">
            <span className="text-primary">SF</span>
            <span className="Logo__tinyText"> </span>Endorsements
          </span>
        </Link>
      </h1>
    );
  }
}
