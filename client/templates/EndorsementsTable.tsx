import React from 'react';
import _ from 'lodash';
import { Organization, RaceMetadata } from '../../common/types';
import EndorsementRow from './EndorsementRow';
import RaceName from './RaceName';

interface Props {
  /** ID prefix for popovers, etc. contained in this table */
  tableId: string;
  organizations: Organization[];
  races: RaceMetadata[];
  linkKey: 'link' | 'propositionsLink';
}

const colors = [
  '#6e4731',
  '#686e31',
  '#2c6136',
  '#2c5161',
  '#2d2c61',
  '#873d7d',
  '#873d47',
  '#6e5e31',
  '#49873d',
  '#2c615d',
  '#2c3d61',
  '#5b3d87',
  '#612c46',
];

/** Display the endorsements for a subset of races and/or organizations */
export default class EndorsementsTable extends React.PureComponent<Props> {
  render() {
    const { organizations, races, tableId, linkKey } = this.props;

    const names: string[] = _.uniq(
      _.flattenDeep(
        organizations.map((org) =>
          races.map((race) => org.endorsements[race.race] as string),
        ),
      ).filter((name) => !!name),
    );

    const colorMap: { [name: string]: string } = {};
    for (let i = 0; i !== names.length; i++) {
      colorMap[names[i]] = colors[i % colors.length];
    }

    return (
      <table className="table">
        <thead>
          <tr
            // zIndex is helpful so that it appears above the sticky
            // column in EndorsementRow
            style={{ position: 'sticky', top: 0, zIndex: 1 }}
            className="bg-white EndorsementsTable__firstRow"
          >
            <th className="border-bottom">Organization</th>
            {races.map((race) => (
              <th key={race.race} className="border-bottom">
                <RaceName race={race} idSuffix={tableId} />
              </th>
            ))}
          </tr>
        </thead>

        <tbody>
          {organizations.map((organization) => (
            <EndorsementRow
              key={organization.name}
              organization={organization}
              races={races}
              tableId={tableId}
              linkKey={linkKey}
              colorMap={colorMap}
            />
          ))}
        </tbody>
      </table>
    );
  }
}
