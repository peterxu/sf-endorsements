import React from 'react';
import MainPage from './MainPage';
import Logo from './Logo';

interface Props {
  // children included by default
}

/** A MainPage with a white logo that takes you back home */
export default class MainPageWithHeader extends React.PureComponent<
  React.PropsWithChildren<Props>
> {
  render() {
    return (
      <MainPage>
        <div className="container mt-4">
          <Logo />
        </div>
        {this.props.children}
      </MainPage>
    );
  }
}
