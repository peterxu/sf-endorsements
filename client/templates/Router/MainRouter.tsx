import React from 'react';
import { ScrollContext } from 'react-router-scroll-4';
import { HelmetProvider } from 'react-helmet-async';
import Routes from './Routes';
import DelayedBrowserRouter from './DelayedBrowserRouter';
import { routes } from './routeDeclarations';
import DefaultHead from './DefaultHead';

interface MainRouterProps {}

// The react-router-scroll-4 types are not compatible with the latest
// react-router-dom types where children isn't assumed to be a prop
const ScrollContextAny = ScrollContext as any;

const MainRouter: React.FunctionComponent<MainRouterProps> = React.memo(
  function MainRouter() {
    const [loading, setLoading] = React.useState(false);

    return (
      <DelayedBrowserRouter routes={routes} onLoadingChange={setLoading}>
        <ScrollContextAny>
          <>
            <HelmetProvider>
              <DefaultHead />
              <Routes loading={loading} />
            </HelmetProvider>
          </>
        </ScrollContextAny>
      </DelayedBrowserRouter>
    );
  },
);

export default MainRouter;
