import React from 'react';
import loadable from '@loadable/component';
import { RouteProps } from 'react-router';
import LoadingPage from '../LoadingPage';

//
// The /* #__LOADABLE__ */ here are hints for @loadable/babel-plugin to
// do a transform
//

const HomePage = /* #__LOADABLE__ */ () => import('../HomePage');

const Nov2019 = /* #__LOADABLE__ */ () => import('../Past/Nov2019');
const Mar2020 = /* #__LOADABLE__ */ () => import('../Past/Mar2020');
const Nov2020 = /* #__LOADABLE__ */ () => import('../Past/Nov2020');
const Feb2022 = /* #__LOADABLE__ */ () => import('../Past/Feb2022');
const Jun2022 = /* #__LOADABLE__ */ () => import('../Past/Jun2022');
const Nov2022 = /* #__LOADABLE__ */ () => import('../Past/Nov2022');
const Mar2024 = /* #__LOADABLE__ */ () => import('../Past/Mar2024');
const Nov2024 = /* #__LOADABLE__ */ () => import('../Past/Nov2024');
export interface OurRouteProps extends RouteProps {
  /** The page name used for analytics */
  name: string;
  /** Function for loading the page asynchronously using async imports */
  loadFunction: (() => Promise<any>) | null;
}

/** An asynchronous route not bundled with the main bundle */
function route(
  path: string,
  loadFunction: () => Promise<any>,
  name: string,
  options: { exact?: boolean } = {},
): OurRouteProps {
  return {
    ...options,
    path,
    component: loadable(loadFunction, {
      fallback: <LoadingPage />,
    }),
    name,
    loadFunction,
  };
}

export const routes: OurRouteProps[] = [
  route('/', HomePage, 'HomePage', { exact: true }),
  route('/index', HomePage, 'HomePage'),
  route('/past/november-2019-sf-election-endorsements', Nov2019, 'Nov2019'),
  route('/past/march-2020-sf-election-endorsements', Mar2020, 'Mar2020'),
  route('/past/november-2020-sf-election-endorsements', Nov2020, 'Nov2020'),
  route('/past/february-2022-sf-election-endorsements', Feb2022, 'Feb2022'),
  route('/past/june-2022-sf-election-endorsements', Jun2022, 'Jun2022'),
  route('/past/november-2022-sf-election-endorsements', Nov2022, 'Nov2022'),
  route('/past/march-2024-sf-election-endorsements', Mar2024, 'Mar2024'),
  route('/past/november-2024-sf-election-endorsements', Nov2024, 'Nov2024'),
  route('*', HomePage, 'HomePage'),
];
