import React from 'react';
import _ from 'lodash';
import { Organization, Section } from '../../common/types';
import EndorsementsTable from './EndorsementsTable';
import OrganizationsTable from './OrganizationsTable';
import Head from './Head';
import { linkHost } from '../../common/constants';

export interface EndorsementsProps {
  /** Title of the election: e.g., "March 2020" */
  electionTitle: string;
  sections: Section[];
  organizations: Organization[];
}

/** Tables summarizing all endorsements for a single election cycle */
export default class Endorsements extends React.PureComponent<EndorsementsProps> {
  render() {
    const { electionTitle, sections, organizations } = this.props;

    return (
      <>
        <Head
          isArticle
          title={`${electionTitle} SF election endorsements`}
          thumbnailUrl={`${linkHost}/assets/HomePage__sanFrancisco.jpg`}
          description={`Summary of endorsements for San Francisco & California candidates and propositions from ${organizations.length} community organizations`}
        />

        {sections.map(({ heading, subtitle, metadata }) => (
          <React.Fragment key={heading}>
            <h2>{heading}</h2>
            {subtitle && <p>{subtitle}</p>}
            <EndorsementsTable
              tableId={_.camelCase(heading)}
              organizations={organizations}
              races={metadata}
              linkKey="link"
            />
          </React.Fragment>
        ))}

        <hr />

        <h2 className="mt-5">About the endorsers</h2>

        <OrganizationsTable organizations={organizations} />
      </>
    );
  }
}
