import React from 'react';
import { Endorsement } from '../../common/types';

interface Props {
  endorsement: Endorsement;

  /** Map of candidate name to color, for easy browsing */
  colorMap: { [name: string]: string };
}

/** Display an endorsement of either one or more candidates */
export default class EndorsementDisplay extends React.PureComponent<Props> {
  renderEndorsement(name: string) {
    const { colorMap } = this.props;
    if (name === 'Yes') {
      return <strong className="text-success">Yes</strong>;
    } else if (name === 'No') {
      return <strong className="text-danger">No</strong>;
    } else {
      const color = colorMap[name];
      return <strong style={color ? { color } : {}}>{name}</strong>;
    }
  }

  render() {
    const { endorsement } = this.props;
    if (endorsement instanceof Array) {
      return endorsement.map((name, index) => (
        <span key={name}>
          {index !== 0 && <br />}
          {this.renderEndorsement(name)}
        </span>
      ));
    } else {
      return this.renderEndorsement(endorsement);
    }
  }
}
