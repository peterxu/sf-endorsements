import { ElectionData } from '../../common/types';

const electionData: ElectionData = {
  sections: [
    {
      heading: 'Special elections',
      metadata: [
        {
          race: 'stateAssembly17',
          name: 'State Assembly District 17',
        },
        {
          race: 'assessorRecorder',
          name: 'Assessor-Recorder',
        },
      ],
    },
    {
      heading: 'San Francisco ballot measures and propositions (recalls)',
      metadata: [
        {
          race: 'propA',
          name: 'Prop A (recall Collins)',
          title: 'Recall Alison Collins from School Board',
          link: 'https://voterguide.sfelections.org/en/recall-measure-regarding-alison-collins',
          description:
            'Remove Alison Collins from the San Francisco Unified School District School Board and allow the Mayor to appoint a replacement',
        },
        {
          race: 'propB',
          name: 'Prop B (recall López)',
          title: 'Recall Gabriela López from School Board',
          link: 'https://voterguide.sfelections.org/en/recall-measure-regarding-gabriela-l%C3%B3pez',
          description:
            'Remove Gabriela López from the San Francisco Unified School District School Board and allow the Mayor to appoint a replacement',
        },
        {
          race: 'propC',
          name: 'Prop C (recall Moliga)',
          title: 'Recall Faauuga Moliga from School Board',
          link: 'https://voterguide.sfelections.org/en/recall-measure-regarding-faauuga-moliga',
          description:
            'Remove Faauuga Moliga from the San Francisco Unified School District School Board and allow the Mayor to appoint a replacement',
        },
      ],
    },
  ],
  endorsements: {
    'SF Berniecrats': {
      endorsements: {
        stateAssembly17: 'David Campos',
        propA: 'No',
        propB: 'No',
        propC: 'No',
      },
      link: 'https://sfberniecrats.com/',
      hasNonPoliticalActivities: false,
      hasReasoning: true,
    },
    'SF Tenants Union': {
      endorsements: {
        stateAssembly17: 'David Campos',
      },
      link: 'https://sftu.org/endorsements/',
      hasNonPoliticalActivities: true,
      hasReasoning: true,
    },
    'SF League of Pissed Off Voters': {
      endorsements: {
        stateAssembly17: ['David Campos', 'Matt Haney'],
        propA: 'No',
        propB: 'No',
        propC: 'No',
      },
      link: 'http://www.theleaguesf.org/',
      hasNonPoliticalActivities: false,
      hasReasoning: true,
    },
    'SF League of Conservation Voters': {
      endorsements: {
        propA: 'Yes',
        propB: 'No',
        propC: 'No',
      },
      link: 'https://www.sflcv.org/blog/2022/1/13/february-2022-recall-alison-collins-only-yes-on-a-no-on-b-amp-c',
      hasNonPoliticalActivities: false,
      hasReasoning: true,
    },
    'YIMBY Action': {
      endorsements: {
        stateAssembly17: ['Bilal Mahmood'],
      },
      link: 'https://yimbyaction.org/endorsements/',
      hasNonPoliticalActivities: false,
      hasReasoning: true,
    },
    "SF Women's Political Committee": {
      endorsements: {
        stateAssembly17: 'Bilal Mahmood',
        assessorRecorder: 'Joaquin Torres',
        propA: 'Yes',
        propB: 'Yes',
        propC: 'No',
      },
      link: 'https://sfwpc.org/endorsements/',
      hasNonPoliticalActivities: false,
      hasReasoning: false,
    },
    'SF Bicycle Coalition': {
      endorsements: {
        stateAssembly17: 'Matt Haney',
      },
      link: 'https://sfbike.org/news/our-endorsement-february-15-2022-elections/',
      hasNonPoliticalActivities: true,
      hasReasoning: true,
    },
    'SF Republican Party': {
      endorsements: {
        propA: 'Yes',
        propB: 'Yes',
        propC: 'Yes',
      },
      link: 'https://www.sfgop.org/ad17info',
      hasNonPoliticalActivities: false,
      hasReasoning: false,
    },
    'District 11 Democratic Club': {
      endorsements: {
        stateAssembly17: 'David Campos',
        assessorRecorder: 'Joaquin Torres',
        propA: 'No',
        propB: 'No',
        propC: 'No',
      },
      link: 'https://www.sfd11dems.com/endorsements',
      hasNonPoliticalActivities: false,
      hasReasoning: true,
    },
    'Potrero Hill Democratic Club': {
      endorsements: {
        assessorRecorder: 'Joaquin Torres',
        propA: 'Yes',
        propB: 'Yes',
      },
      link: 'https://www.phdemclub.org/?page_id=5166',
      hasNonPoliticalActivities: false,
      hasReasoning: false,
    },
    'Harvey Milk LGBTQ Democratic Club': {
      endorsements: {
        stateAssembly17: 'David Campos',
        assessorRecorder: 'Joaquin Torres',
        propB: 'No',
        propC: 'No',
      },
      link: 'http://www.milkclub.org/endorsements',
      hasNonPoliticalActivities: false,
      hasReasoning: true,
    },
    'United Democratic Club': {
      endorsements: {
        assessorRecorder: 'Joaquin Torres',
        propA: 'Yes',
        propB: 'Yes',
        propC: 'Yes',
      },
      link: 'https://www.uniteddems.org/february-2022-elections-endorsements',
      hasNonPoliticalActivities: false,
      hasReasoning: false,
    },
    'Democratic Party (SF)': {
      endorsements: {
        assessorRecorder: 'Joaquin Torres',
      },
      link: 'https://www.sfdemocrats.org/voting/endorsements/2020/02/15/specialelection',
      hasNonPoliticalActivities: false,
      hasReasoning: false,
    },
    'Ed M. Lee Asian Pacific Democratic Club': {
      endorsements: {
        stateAssembly17: ['Bilal Mahmood', 'Matt Haney'],
        assessorRecorder: 'Joaquin Torres',
        propA: 'Yes',
        propB: 'Yes',
        propC: 'Yes',
      },
      link: 'https://www.edleedems.org/endorsements/nov2020',
      hasNonPoliticalActivities: false,
      hasReasoning: true,
    },
    'Alice B. Toklas LGBT Democratic Club': {
      endorsements: {
        assessorRecorder: 'Joaquin Torres',
        propA: 'Yes',
        propB: 'Yes',
      },
      link: 'https://www.alicebtoklas.org/endorsements',
      hasNonPoliticalActivities: false,
      hasReasoning: false,
    },
    'Bernal Heights Democratic Club': {
      endorsements: {
        stateAssembly17: 'David Campos',
        assessorRecorder: 'Joaquin Torres',
        propC: 'No',
      },
      link: 'http://bhdemocrats.org/index.php/endorsements/',
      hasNonPoliticalActivities: false,
      hasReasoning: false,
    },
    'San Francisco Bay Guardian': {
      endorsements: {
        stateAssembly17: 'David Campos',
        assessorRecorder: 'Joaquin Torres',
        propA: 'No',
        propB: 'No',
        propC: 'No',
      },
      link: 'https://www.sfbg.com/2022/01/20/endorsements-for-the-february-2022-election/',
      hasNonPoliticalActivities: false,
      hasReasoning: true,
    },
    'Chinese American Democratic Club': {
      endorsements: {
        stateAssembly17: ['Matt Haney', 'Bilal Mahmood'],
        assessorRecorder: 'Joaquin Torres',
        propA: 'Yes',
        propB: 'Yes',
        propC: 'Yes',
      },
      link: 'https://www.sfcadc.org/current_endorsements',
      hasNonPoliticalActivities: false,
      hasReasoning: false,
    },
    'San Francisco Chronicle': {
      endorsements: {
        stateAssembly17: 'Bilal Mahmood',
        propA: 'Yes',
        propB: 'Yes',
        propC: 'Yes',
      },
      link: 'https://www.sfchronicle.com/sf/article/Here-s-everything-you-need-to-know-about-San-16779048.php',
      hasNonPoliticalActivities: true,
      hasReasoning: true,
    },
    'Eastern Neighborhoods Democratic Club': {
      endorsements: {
        stateAssembly17: 'Bilal Mahmood',
        assessorRecorder: 'Joaquin Torres',
        propA: 'Yes',
        propB: 'Yes',
      },
      link: 'https://www.sfendc.com/endorsements',
      hasNonPoliticalActivities: false,
      hasReasoning: false,
    },
    'San Francisco Examiner': {
      endorsements: {
        stateAssembly17: 'Matt Haney',
        propA: 'Yes',
        propB: 'Yes',
        propC: 'Yes',
      },
      link: 'https://www.sfexaminer.com/opinion/endorsement-san-franciscos-school-board-is-a-national-laughingstock-yes-on-the-recall/',
      hasNonPoliticalActivities: false,
      hasReasoning: true,
    },
    'SF Housing Action Coalition': {
      endorsements: {
        propA: 'Yes',
        propB: 'Yes',
        propC: 'Yes',
      },
      link: 'https://www.sfhac.org/why-urbanists-should-vote-yes-on-san-franciscos-board-of-education-recall-campaign/',
      hasNonPoliticalActivities: false,
      hasReasoning: true,
    },
    GrowSF: {
      endorsements: {
        stateAssembly17: 'Bilal Mahmood',
        assessorRecorder: 'Joaquin Torres',
        propA: 'Yes',
        propB: 'Yes',
        propC: 'Yes',
      },
      link: 'https://growsf.org/voter-guide/',
      hasNonPoliticalActivities: false,
      hasReasoning: true,
    },
  },
};
export default electionData;
