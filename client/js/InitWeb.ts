import '../style/Styles.scss';

// Babel polyfills
import 'core-js/stable';
import 'regenerator-runtime/runtime';

import React from 'react';
import { hydrateRoot, createRoot } from 'react-dom/client';
import { loadableReady } from '@loadable/component';

import MainRouter from '../templates/Router/MainRouter';
import { ClientConfig } from '../../common/types';

declare global {
  interface Window {
    __CONFIG__: ClientConfig;
  }
}

const useServerSideRendering = window.__CONFIG__.shouldServerSideRender;
const reactMain = document.getElementById('react-main');

if (reactMain) {
  if (useServerSideRendering) {
    loadableReady(() => {
      hydrateRoot(reactMain, React.createElement(MainRouter));
    });
  } else {
    document.addEventListener('DOMContentLoaded', () => {
      // On the client, we don't do server-side rendering,
      // so just do a plain render
      createRoot(reactMain).render(React.createElement(MainRouter));
    });
  }
}
