// Global styles must come first
import '../client/templates/FontAwesome';
import '../client/style/Styles.scss';

import React from 'react';
import { HelmetProvider } from 'react-helmet-async';
import StoryRouter from 'storybook-react-router';
import { Preview } from '@storybook/react';

const ProvidersDecorator = Story => {
  return (
    <HelmetProvider>
      <Story />
    </HelmetProvider>
  );
};

export const decorators = [ProvidersDecorator, StoryRouter()];

const preview: Preview = {
  parameters: {
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
    layout: 'fullscreen',
  },
};

export default preview;