import type { StorybookConfig } from '@storybook/react-webpack5';
import autoprefixer from 'autoprefixer';
import path from 'path';

const browsersListBrowsers = ['>0.5% in US', 'not Android >0'];

const config: StorybookConfig = {
  stories: [
    '../client/**/*.stories.@(js|jsx|mjs|ts|tsx)',
  ],
  staticDirs: ['../client'],
  addons: [
    '@storybook/addon-webpack5-compiler-swc',
    '@storybook/addon-onboarding',
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@chromatic-com/storybook',
    '@storybook/addon-interactions',
  ],
  framework: {
    name: '@storybook/react-webpack5',
    options: {},
  },
  typescript: {
    reactDocgen: 'react-docgen-typescript',
  },
  webpackFinal: async (config) => {
    if (config.module && config.module.rules) {
      config.module.rules.push({
        test: /\.scss$/,
        use: [
          'style-loader',
          { loader: 'css-loader', options: { url: false } },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [autoprefixer({ overrideBrowserslist: browsersListBrowsers })],
              },
            },
          },
          'sass-loader',
        ],
        include: path.resolve(__dirname, '..'),
      });
    }

    if (config.resolve) {
      config.resolve.extensions?.push('.ts', '.tsx');
    }

    return config;
  },
};

export default config;
