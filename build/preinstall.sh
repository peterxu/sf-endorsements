#!/bin/sh
set -e

# This script is meant to be run by copy/pasting its contents into the
# terminal on a fresh Ubuntu server.
#
# Then, run `bash preinstall.sh`

cat > preinstall.sh <<'EOS'
#!/bin/sh
set -e

PERSONAL_ACCESS_TOKEN="$1"

# If another argument is passed, we'll use it as extra repositories to add our
# deploy key to
EXTRA_REPOSITORIES="$2"

while [ -z "$PERSONAL_ACCESS_TOKEN" ]; do
  echo 'Generate a temporary full API personal access token in Gitlab at'
  echo 'https://gitlab.com/-/user_settings/personal_access_tokens'
  echo 'then enter it here:'
  echo ''
  read -e PERSONAL_ACCESS_TOKEN
done

# Make sure all apt commands are non-interactive
# with DEBIAN_FRONTEND, force-confdef and force-confold
export DEBIAN_FRONTEND=noninteractive
APT_ARGS=(-y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -o DPkg::Lock::Timeout=300)
# We add DPkg::Lock::Timeout because sometimes, on initialization of a server,
# it'll run automatic updates which will hold the dpkg lock for a while.

# Disable unattended upgrades: they often use dpkg.lock
if [ -f /etc/apt/apt.conf.d/20auto-upgrades ]; then
  sed -i 's/APT::Periodic::Unattended-Upgrade "1";/APT::Periodic::Unattended-Upgrade "0";/' /etc/apt/apt.conf.d/20auto-upgrades
fi

# Disable password authentication for security
sed -i 's@.*PasswordAuthentication.*@PasswordAuthentication no@i' /etc/ssh/sshd_config

# Note: we use service ssh restart because it works on a wider range of systems:
# systemctl restart ssh doesn't work on Docker:
# > System has not been booted with systemd as init system (PID 1).
service ssh restart

# Update installed packages
apt-get update "${APT_ARGS[@]}" && apt-get upgrade "${APT_ARGS[@]}"

# For unknown reasons, installing jq is often a bit flaky, so we try it twice
apt-get install "${APT_ARGS[@]}" jq curl ufw vim || apt-get install "${APT_ARGS[@]}" jq curl ufw vim

#
# This script creates the account for web and pulls
# the code to bootstrap installation of everything else
#

if [ ! -f /root/.ssh/id_ed25519 ]; then
  ssh-keygen -t ed25519 -N '' -f /root/.ssh/id_ed25519
fi

# Randomly generate a password for the web account, since
# we only use `su` to access the account
# https://unix.stackexchange.com/questions/230673/how-to-generate-a-random-string
NODE_WEB_PASS=$(</dev/urandom tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-./:;<=>?@[\]^_`{|}~' | head -c 32 ; echo)

if [ -z "$(id web 2> /dev/null || true)" ]; then
  useradd --home /home/web --shell /bin/bash --create-home web
fi
echo "web:$NODE_WEB_PASS" | chpasswd

#
# Add this server’s deploy keys to the right repositories
#
KEY_FILE=/root/.ssh/id_ed25519.pub

SSH_KEY=`head -n 1 $KEY_FILE`
HOSTNAME=`hostname`

echo 'Getting projects list…'

for REPOSITORY in peterxu/sf-endorsements myrtlelime/install-scripts $EXTRA_REPOSITORIES; do
  RESULT=$(
    curl \
      --request GET \
      --header "Private-Token: $PERSONAL_ACCESS_TOKEN" \
      --header "Content-Type: application/json" \
      "https://gitlab.com/api/v4/projects?visibility=private&simple=true&membership=true&search_namespaces=true&search=$REPOSITORY" | jq -r '.[] | "\(.name) \(.id)"' | head
  )
  # We need search_namespaces=true above because we search "travelchime/XXX",
  # and the travelchime is in the namespace

  PROJECT_IDS=$(awk '{print $2}' <<< "$RESULT" | tr '\n' ' ')

  # Just in case each search returns multiple results, add the deploy
  # key to all of them so that we are guaranteed to have it on the right repo
  for PROJECT_ID in $PROJECT_IDS; do
    echo
    echo "Adding deploy key to $REPOSITORY ($PROJECT_ID)"
    JSON_DATA='{"title": "'"$HOSTNAME"'", "key": "'"$SSH_KEY"'", "can_push": false}'

    curl \
      --request POST \
      --header "Private-Token: $PERSONAL_ACCESS_TOKEN" \
      --header "Content-Type: application/json" \
      --data "$JSON_DATA" \
      "https://gitlab.com/api/v4/projects/$PROJECT_ID/deploy_keys/"
  done
done

apt-get install "${APT_ARGS[@]}" git
ssh-keyscan gitlab.com >> /root/.ssh/known_hosts
cd /home/web

# Make these idempotent: if any of these lines fail, it's likely that
# the repository already exists. Just make sure we have the latest
git clone git@gitlab.com:peterxu/sf-endorsements.git app || true
cd app || exit 1
git fetch && git reset --hard origin/master

cd /root
git clone git@gitlab.com:myrtlelime/install-scripts.git || true
cd install-scripts || exit 1
git fetch && git reset --hard origin/master

# Install unattended upgrades - some systems don't install it by default
apt-get install "${APT_ARGS[@]}" unattended-upgrades
# Ensure unattended upgrades is enabled
if [ -f /etc/apt/apt.conf.d/20auto-upgrades ]; then
  sed -i 's/APT::Periodic::Unattended-Upgrade "0";/APT::Periodic::Unattended-Upgrade "1";/' /etc/apt/apt.conf.d/20auto-upgrades
fi
EOS
