#!/bin/sh
cd /root/install-scripts || exit 1
bash components/nginx.sh
bash components/nodejs.sh 20
bash components/yarn.sh


bash components/pre-letsencrypt.sh
bash components/letsencrypt.sh sfendorsements.com www.sfendorsements.com
bash components/post-letsencrypt.sh

sed -i 's@DOMAIN@sfendorsements.com@gi' /etc/nginx/sites-available/default
sed -i 's@proxy_pass.*@proxy_pass http://localhost:61987;@gi' /etc/nginx/sites-available/default

cd /home/web || exit 1
git clone git@gitlab.com:peterxu/sf-endorsements.git app

cd /home/web/app || exit 1
yarn install
yarn build

su -c 'NODE_ENV=production DATABASE=production pm2 start node -- /home/web/app/dist/app/server/bin/www.js' web
su -c 'pm2 save' web
