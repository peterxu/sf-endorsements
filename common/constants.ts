/** The name of the site */
export const siteName = 'SFEndorsements';

/** The default meta description for the site */
export const defaultDescription =
  'Summary of endorsements for all San Francisco candidates and ballot propositions from across the political spectrum';

/**
 * The host for links (without trailing slash), for use in non-relative
 * links (e.g., in emails)
 */
export const linkHost = 'https://sfendorsements.com';

/** Tag line to put after the siteName in page titles */
export const tagline = 'San Francisco election endorsements summary';

/** The Facebook page for the publisher */
export const facebookPageLink: string | null = null;

/** Name or corporation of the site admin */
export const adminName = 'Peter Xu';

/** Email to get in touch with the creator */
export const adminEmail = 'peter@wanderlog.com';
