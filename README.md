# SF Endorsements code

This is the code for SFEndorsements.com, a project that tries to summarize the
endorsements for San Francisco local, state, and federal elections.

## How to update

To update the site for a new election:

1. Move the code on the `client/templates/HomePage.tsx` to `client/templates/Past/MonthYear.tsx` (e.g., `client/templates/Past/Mar2020.tsx`)
2. Make a copy of this spreadsheet template on Google Sheets: https://docs.google.com/spreadsheets/d/1f3cGgXjI912nA8e1_iTlEhJ7XpxjpSKZdT7wX3ryztg/edit
3. Fill it in with this election's endorsements. Optionally, you can ask someone else to do this [following the instructions in this Google Doc](https://docs.google.com/document/d/1fdl6yLFZyIDTnZ3VQynSea8_iSlmo-VMXxE8qYlju60/edit)
4. Download the spreadsheet as a .xlsx (Excel) file
5. Run `yarn b-node server/scripts/importTemplates.ts NAME_OF_DOWNLOADED_FILE.xlsx > client/data/MonthYear.ts`
6. Import that data from the home page, and use it as the `data` prop of `<ElectionEndorsements />`
7. Rotate the elections:
   - [ ] Add a file like e.g., client/templates/Past/Nov2022.tsx
   - [ ] Import it in routeDeclarations.tsx
   - [ ] Change the HomePage so it links to the past election
8. Change HomePage to use the latest data
   - [ ] Change electionTitle

To update the server, SSH in, and run:

```sh
cd /home/web/app
git pull
yarn install
yarn build
su -c 'pm2 restart all' web
```
