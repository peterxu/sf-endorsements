import process from 'process';
import * as winston from 'winston';
import config from '../config';

type LogFunction = (message: string, ...meta: any[]) => any;

export interface Logger {
  log: LogFunction;
  info: LogFunction;
  error: LogFunction;
  debug: LogFunction;
}

function createLogger(logPath: string) {
  const transports: winston.transport[] = [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.simple(),
      ),
    }),
  ];

  if (config.isProduction) {
    transports.push(new winston.transports.File({ filename: logPath }));
  }

  const logLevel = process.env.WINSTON_LOGLEVEL;
  const winstonConfig: winston.LoggerOptions = { transports };
  if (logLevel) {
    winstonConfig.level = logLevel;
  }
  return winston.createLogger(winstonConfig);
}

export const logger = createLogger(config.logPath);
export const jobsLogger = createLogger(config.jobsLogPath);

/** Close loggers to stop holding onto file handles */
export function closeLoggers() {
  logger.close();
  jobsLogger.close();
}

export default { logger, closeLoggers };
