import express, { Request, Response } from 'express';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { ChunkExtractor } from '@loadable/server';
import path from 'path';
import fs from 'fs';
import { HelmetProvider, HelmetServerState } from 'react-helmet-async';

import { logger } from '../libs/logger';
import '../../client/templates/FontAwesome';
import Routes from '../../client/templates/Router/Routes';
import config from '../config';
import DefaultHead from '../../client/templates/Router/DefaultHead';
import { logError } from '../../common/logging';
import { ClientConfig } from '../../common/types';

//
// For @loadable/component: loads all code-split asynchronous includes
// on the server side before returning to client
//
const statsFile = path.join(__dirname, '../public/js/loadable-stats.json');
let splitCodeExtractor: ChunkExtractor | null = null;
if (config.shouldServerSideRender) {
  if (fs.existsSync(statsFile)) {
    splitCodeExtractor = new ChunkExtractor({ statsFile });
  } else {
    logger.warn(
      "Server-side rendering is on, but we couldn't find loadable-stats.json to make sure code-split components are loaded",
    );
    splitCodeExtractor = null;
  }
}

const router = express.Router();

async function handleRequest(req: Request, res: Response) {
  try {
    res.type('text/html');

    const context: any = {};
    const helmetContext: {
      helmet?: HelmetServerState;
    } = {} as any;

    let app = (
      // <Provider store={store}>
      <StaticRouter location={req.originalUrl} context={context}>
        <HelmetProvider context={helmetContext}>
          <DefaultHead />
          <Routes loading={false} />
        </HelmetProvider>
      </StaticRouter>
      // </Provider>
    );
    let scriptTags = '';

    if (splitCodeExtractor) {
      app = splitCodeExtractor.collectChunks(app);
      scriptTags = splitCodeExtractor.getScriptTags();
    }

    const rendered = renderToString(app);

    res.status(context.is404 ? 404 : 200);

    if (context.url) {
      res.redirect(302, context.url);
    } else {
      const helmet = helmetContext.helmet!;

      res.render('index', {
        title: helmet.title.toString(),
        meta: helmet.meta.toString(),
        link: helmet.link.toString(),
        style: helmet.style.toString(),
        script: helmet.script.toString(),
        scriptTags,
        noscript: helmet.noscript.toString(),
        // Only do server-side rendering in production
        react: config.shouldServerSideRender ? rendered : '',
        bodyAttributes: helmet.bodyAttributes.toString(),

        config: {
          shouldServerSideRender: config.shouldServerSideRender,
          isProduction: config.isProduction,
          googleAnalyticsId: config.googleAnalyticsId,
        } as ClientConfig,

        baseUrl: '',
      });
    }
  } catch (err) {
    logError(logger.error, err);
    res
      .status(500)
      .send('An unexpected error occurred; please try again later');
  }
}

router.get('*', handleRequest);

export default router;
